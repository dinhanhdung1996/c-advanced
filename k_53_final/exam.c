#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfdr/jrb.h>
#include <libfdr/jval.h>
#include <libfdr/dllist.h>
#include <libgraph/graph.h>
#include <libllist/queue.h>
#define MAX_LENGTH 100


void readInData(Graph graph, char *fileName)
{
  FILE *f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("File error\n");
      return;
    };

  char *buffer = (char *)malloc(sizeof(char) * 100);
  char *token = (char *)malloc(sizeof(char) * 100);
  char *s = " ->\n";
  int nodeIncrement = 0;

  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      if(token == NULL)break;
      int node1 = getVertexID(graph, token);
      if(node1 == -1)
	{
	  node1 = nodeIncrement;
	  nodeIncrement++;
	  addVertex(graph, node1, token);
	};

      token = strtok(NULL, s);
      if(token == NULL)continue;
      int node2 = getVertexID(graph, token);
      if(node2 == -1)
	{
	  node2 = nodeIncrement;
	  nodeIncrement++;
	  addVertex(graph, node2, token);
	};
      addEdgeUndir(graph, node1, node2, 1);
    };
  fclose(f);
}


void printStatus(Graph graph, int vertex)
{
  int *output = (int *)malloc(sizeof(int) * 100);
  int n = outdegree(graph, vertex, output);
  printf(" %s has %d friend are: ", getVertex(graph, vertex), n);
  int i;
  for(i = 0; i < n; i++)
    {
      printf("%s, ", getVertex(graph, output[i]));
    };
  printf("\n");
}

JRB getBFSLevelTree(Graph graph, JRB tree, Queue queue, int level, int *visit)
{
  if(dll_empty(queue))
    {
      return tree;
    };
  if(visit == NULL)
    {
      visit = (int *)malloc(sizeof(int) * MAX_LENGTH);
      int j;
      for(j = 0; j < MAX_LENGTH ; j++)
	{
	  visit[j] = 0;
	};
    };

  level += 1;
  
  JRB subTree = jrb_find_int(tree, level);
  if(subTree == NULL)
    {
      jrb_insert_int(tree, level, new_jval_v(make_jrb()));
    }

  Dllist temp;
  dll_traverse(temp, queue)
    {
      visit[jval_i(temp->val)] = 1;
    };

  subTree = (JRB)jval_v(jrb_find_int(tree, level)->val);
  Dllist nextLevel = new_dllist();
  while( !dll_empty(queue))
    {
      int node = deQueue(queue);
      visit[node] = 1;
      int *output = (int *)malloc(sizeof(int) * MAX_LENGTH);
      int n = outdegree(graph, node, output);
      int i;
      for(i = 0; i< n; i++)
	{
	  if(!visit[output[i]])
	    {
	      jrb_insert_int(subTree, output[i], new_jval_i(1));
	      enQueue(nextLevel, output[i]);
	    }
	};
    }
  getBFSLevelTree(graph, tree, nextLevel, level, visit);
  return tree;
}

JRB getLevelTree(Graph graph, int vertex)
{
  Queue queue = createQueue();
  enQueue(queue, vertex);
  int *visit = NULL;
  JRB tree = make_jrb();
  getBFSLevelTree(graph, tree, queue, 0, visit);
  return tree;
}

void printLevelTree(Graph graph, int vertex)
{
  JRB tree = getLevelTree(graph, vertex);
  printf("%d\n", tree);
  JRB node = jrb_find_int(tree, 1);
  if(node == NULL)printf("false\n");
  printf("%d\n", vertex);
  JRB ptr;
  jrb_traverse(ptr, tree)
    {
      
      JRB subTree = (JRB) jval_v(ptr->val);
      if(jrb_empty(subTree))continue;
      JRB ptr2;
      printf("level %d : ", jval_i(ptr->key));
      jrb_traverse(ptr2, subTree)
	{
	  printf("%s, ", getVertex(graph, jval_i(ptr2->key)));
	};
      printf("\n");
    }
}


int main()
{
  int choice;
  Graph graph = createGraph();

  do
    {
      printf("Menu \n");
      printf("1. Read in data\n");
      printf("2. Display number of people and their names\n");
      printf("3. Display the relationship \n");
      printf("4. Print list of all personels and status\n");
      printf("5. Personal Information\n");
      
      printf("your choice : ");
      scanf("%d", &choice);
      while(getchar() != '\n');

      if(choice == 1)
	{
	  printf("Input file name: ");
	  char *fileName = (char *)malloc(sizeof(int) * MAX_LENGTH);
	  gets(fileName);
	  graph = createGraph();
	  readInData(graph, fileName);
	};

      if(choice == 2)
	{
	  int n;
	  printf("Number of people in social network: %d\n", (n=countVerticesGraph(graph)));
	  JRB ptr;
	  int i = 0;
	  jrb_traverse(ptr, graph.vertices)
	    {
	      i++;
	      printf("%d . %s\n", i, jval_s(ptr->val));
	    };
	};

      if(choice == 3)
	{
	  int n;
	  printf("Number of relationship in social network: %d\n", (n=countEdgesGraph(graph)/2));
	};

      if(choice == 4)
	{
	  JRB ptr;
	  jrb_traverse(ptr, graph.vertices)
	    {
	      printStatus(graph, jval_i(ptr->key));
	    }
	}

      if(choice == 5)
	{
	  printf("Input a person: ");
	  char *personName = (char *)malloc(sizeof(char) * 100);
	  gets(personName);
	  int node = getVertexID(graph, personName);
	  if(node == -1)
	    {
	      printf("No person name : %s\n", personName);
	      continue;
	    };
	  printLevelTree(graph, node);
	}

    }while(choice != 6);
  return 0;
}
