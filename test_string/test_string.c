#include <stdio.h>
#include <stdlib.h>
#include <libstring/string_deal.h>


int main()
{
  printf("Input string : test trim left");
  char *name = (char *)malloc(sizeof(char) * 100);
  gets(name);
  printf("abc%scde\n", name);
  trimLeft(name);
  printf("abc%scde\n", name);

  printf("Input string: test trim right");
  char *name2 = (char *)malloc(sizeof(char) * 100);
  gets(name2);
  printf("abc%scde\n", name2);
  trimRight(name2);
  printf("abc%scde\n", name2);

  printf("Input string: test trim middle\n");
  char *name3 = (char *)malloc(sizeof(char) * 100);
  gets(name3);
  printf("abc%scde\n", name3);
  trimMiddle(name3);
  printf("abc%scde\n", name3);


  printf("Input string: test all \n");
  char *name4 = (char *)malloc(sizeof(char) * 100);
  gets(name4);
  printf("abc%scde\n", name4);
  
  trimLeft(name4);
  trimRight(name4);
  trimMiddle(name4);

  printf("ac%scde\n", name4);
  return 0;
}
