#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfdr/jrb.h>
#include <libfdr/jval.h>
#include <libfdr/dllist.h>
#include <libgraph/graph.h>
#define MAX_LENGTH 100


int readInData(Graph graph, char *fileName)
{
  FILE * f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("File unavilable\n");
      return 0;
    };
  int nodeIncrement = 0;
  char *buffer = (char *)malloc(sizeof(char) * MAX_LENGTH);
  char *token = (char *)malloc(sizeof(char)* MAX_LENGTH);
  char *s = " \t:\n";
  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      int nodeID = -1;
      if(token != NULL)
	{
	  nodeID = getVertexID(graph, token);
	  if(nodeID == -1)
	    {
	      addVertex(graph, nodeIncrement, token);
	      nodeID = nodeIncrement;
	      nodeIncrement++;
	    };
	};
      while( (token = strtok(NULL, s) ) != NULL)
	{
	  int sID = getVertexID(graph, token);
	  if(sID == -1)
	    {
	      addVertex(graph, nodeIncrement, token);
	      sID = nodeIncrement;
	      nodeIncrement++;
	    };
	  addEdge(graph, sID, nodeID, 1);
	};
    };
  return 1;
}

void viewPre(Graph graph)
{
  printf("Input the name of the subject to view prerequisite subjects: ");
  char *subjectName = (char*)malloc(sizeof(char) * 100);
  gets(subjectName);
  int nodeID = getVertexID(graph, subjectName);
  if(nodeID == -1)
    {
      printf("Subject code %s is not in curriculum\n", subjectName);
      return;
    }
  else
    {
      int *output = (int *)malloc(sizeof(int) * 100);
      int n = indegree(graph, nodeID, output);
      int i;
      printf("Number of prerequisite subjects is %d\n", n);
      for(i = 0; i< n; i++)
	{
	  printf("%d . %s\n", i+1 ,getVertex(graph, output[i]));
	};
    };
  return;
  
}

Dllist iArrayToDllist(int *array, int n)
{
  int i;
  Dllist dllist = new_dllist();
  for(i = 0; i< n; i++)
    {
      dll_append(dllist, new_jval_i(array[i]));
    };
  return dllist;
}

void insertNodeOrderTree(JRB tree, int n, int key, Dllist dllist)
{
  JRB subTree = jrb_find_int(tree, n);
  if(subTree == NULL){
    subTree = make_jrb();
    jrb_insert_int(tree, n, new_jval_v(subTree));
  };

  subTree = (JRB)jval_v(jrb_find_int(tree, n)->val);
  jrb_insert_int(subTree, key, new_jval_v(dllist));
  
}

void printOrderTree(JRB orderTree, Graph graph)
{
  JRB ptr;
  jrb_traverse(ptr, orderTree)
    {
      printf("%d : ", jval_i(ptr->key));
      JRB subTree = (JRB)jval_v(ptr->val);
      JRB ptr2;
      jrb_traverse(ptr2, subTree)
	{
	  printf("%s (", getVertex(graph, jval_i(ptr2->key)));
	  Dllist dllist = (Dllist) jval_v(ptr2->val);
	  Dllist ptr3;
	  dll_traverse(ptr3, dllist)
	    {
	      printf("%s", getVertex(graph, jval_i(ptr3->val)));
	      if(dll_next(ptr3) != NULL)
		{
		  printf(", ");
		};
	    };
	  printf(") - ");
	}
      printf("\n");
    }
}


JRB getIndegreeOrder(Graph graph)
{
  JRB treeIndegree = make_jrb();
  JRB ptr;
  int i;
  jrb_traverse(ptr, graph.vertices)
    {
      int *output = (int *)malloc(sizeof(int) * 100);
      int n  = indegree(graph, jval_i(ptr->key), output);
      insertNodeOrderTree(treeIndegree, n, jval_i(ptr->key), iArrayToDllist(output, n));
    };
  return treeIndegree;   
}



void printIndegreeOrder(Graph graph)
{
  JRB indegreeTree = getIndegreeOrder(graph);
  printOrderTree(indegreeTree, graph);
}


void printTopo(Graph graph)
{
  Dllist path = topologicalOrder(graph);
  Dllist ptr;
  dll_traverse(ptr, path)
    {
      printf("%s - ", getVertex(graph, jval_i(ptr->val)));
    };
  printf("\n");
}

int main()
{
  int i;
  int j;
  Graph graph = createGraph();

  printf("SUBJECT MANAGEMENT\n");
  printf("-------------------------------------------\n");

  printf("Input the data file : ");
  char *fileName = (char*)malloc(sizeof(char) * 100);
  gets(fileName);

  if( !readInData(graph, fileName)) //Test by test1.txt
    return 0; //TODO
  printf("Number of vertices : %d\n", countVerticesGraph(graph));
  printf("Number of edges : %d\n", countEdgesGraph(graph));
  if( DAG(graph))
    {
      //Test by test2.txt
      printf("Data is unavailable \n");
      return 0;
    };

  int con4 = 0;
  int subChoice = 1;
  do
    {
      if(con4 == 1)
	{
	  printf("Do you want to check prerequisite subjects again\n");
	  printf("1. Yes\n");
	  printf("2. No\n");
	  scanf("%d", &subChoice);
	  while(getchar() != '\n');
	};

      if(subChoice == 1)
	{
	  viewPre( graph); //TODO
	}
      
      con4 = 1;
    }while( subChoice != 2);

  printIndegreeOrder( graph);

  printTopo(graph);

  return 0;

}
  
