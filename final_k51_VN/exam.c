#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgraph/graph.h>
#include <libfdr/jrb.h>
#include <libfdr/dllist.h>
#include <libfdr/jval.h>
#define MAX_ITEM 100


typedef struct Matrix{
  int i;
  int j;
  int nodeID;
  int value;
}Matrix;

Matrix **makeMatrix()
{
  Matrix **matrix = (Matrix **) malloc(sizeof(Matrix*) * MAX_ITEM);
  int i;
  int j;
  for(i = 0; i < MAX_ITEM; i++)
    {
      matrix[i] = (Matrix *)malloc(sizeof(Matrix) * MAX_ITEM);
      for (j = 0; j < MAX_ITEM; j++)
	{
	  matrix[i][j].value = -1;
	};
    };
  return matrix;
}

char *getNodeName(int a, int b)
{
  char *name = (char *)malloc(sizeof(char)*10);
  char *aName = (char *)malloc(sizeof(char)* 10);
  char *bName = (char *)malloc(sizeof(char) * 10);
  strcpy(name, "");
  sprintf(aName, "%d", a);
  sprintf(bName, "%d", b);
  name = strcat(name, aName);
  name = strcat(name, "-");
  name = strcat(name, bName);
  return name;
}



void matchAllEdges(Graph graph, Matrix **matrix, int maxIndexRow, int maxIndexColumn)
{
  if(matrix == NULL)
    return;
  int i;
  int j;
  for(i = 0; i< maxIndexRow; i++)
    {
      for(j = 0; j < maxIndexColumn; j++)
	{
	  if( matrix[i][j].value == 0)
	    {
	      if( j - 1 > 0)
		{
		  if ( matrix[i][j - 1].value == 0)
		    {
		      addEdgeUndir(graph, matrix[i][j].nodeID, matrix[i][j-1].nodeID, 1);
		    };
		};

	      if( j + 1 < maxIndexColumn)
		{
		  if( matrix[i][j + 1].value == 0)
		    {
		      addEdgeUndir(graph, matrix[i][j].nodeID, matrix[i][j-1].nodeID, 1);
		    };
		};

	      if( i - 1 > 0)
		{
		  if( matrix[i - 1][j].value == 0)
		    {
		      addEdgeUndir(graph, matrix[i][j].nodeID, matrix[i - 1][j].nodeID, 1);
		    };
		};

	      if( i + 1 < maxIndexRow)
		{
		  if( matrix[i + 1][j].value == 0)
		    {
		      addEdgeUndir(graph, matrix[i][j].nodeID, matrix[i + 1][j].nodeID, 1);
		    };
		};
	    }
	}
    }	  
}

Matrix **readInData(Graph graph, char *fileName)
{
  FILE * f = fopen(fileName, "r");

  if(f == NULL)
    {
      printf("Error \n");
      return NULL;
    };

  int i = 0;
  int j = 0;
  int nodeID = 0;
  Matrix **matrix = makeMatrix();
  int row;
  int column;

  char *buffer = (char *)malloc(sizeof(char) * 100);
  char *token = (char *)malloc(sizeof(char) * 100);
  char *s = " \n\t";

  while( fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      matrix[i][j].i = i;
      matrix[i][j].j = j;
      matrix[i][j].nodeID = nodeID;
      matrix[i][j].value = atoi(token);
      if(matrix[i][j].value == 0)
	 addVertex(graph, nodeID, getNodeName(i , j));
      
      
      while( (token = strtok(NULL, s)) != NULL)
	{
	  j++;
	  nodeID++;
	  matrix[i][j].i = i;
	  matrix[i][j].j = j;
	  matrix[i][j].nodeID = nodeID;
	  matrix[i][j].value = atoi(token);
	  if(matrix[i][j].value == 0)
	    addVertex(graph, nodeID, getNodeName(i, j));
	};
     
      i++;
      nodeID++;
      column = j+1;
      j = 0;
	          
    };
  row = i;
  matchAllEdges(graph, matrix, row,column);  
  return matrix;
}


void printMatrix(Matrix **matrix)
{
  int i;
  int j;
  for(i = 0; i < MAX_ITEM; i++)
    {
      if(matrix[i][0].value == -1)break;
      for(j = 0; j< MAX_ITEM; j++)
	{
	  if(matrix[i][j].value == -1) break;
	  printf("%d ", matrix[i][j].value);
	};
      printf("\n");
    };
  return;
}


void printValue(Graph graph, int a)
{
  printf("%s\n", getVertex(graph, a));
}




int main()
{
  Graph graph = createGraph();
  Matrix **matrix ;
  int choice;

  do
    {
      printf("MENU\n");
      printf("1. Read in matrix from file \n");
      printf("2. Display the information about the matrix\n");
      printf("3. Find the adjacent vertices to a vertex\n");
      printf("4. Print the list of vertices with the largest number of adjacent vertices\n");
      printf("5. Print the list of node that it can not move to another node\n");
      printf("6. Input 2 points and the smallest path between two those points.\n");
      printf("7. Print out the possible numbers of way to move from a point to another\n");
      printf("Your choice: ");
      scanf("%d", &choice);
      while(getchar() != '\n');



      //Test space
      /* printf("%s\n", getNodeName(1, 2)); */
      /* printf("%s\n", getNodeName(2, 3)); */
      /* printf("%s\n", getNodeName(3, 4)); */
      /* printf("%s\n", getNodeName(40, 50)); */
      if( choice == 1)
	{
	  printf("Input file name: ");
	  char *fileName = (char *)malloc(sizeof(char) * 100);
	  gets(fileName);
	  matrix = readInData(graph, fileName);
	  printf("Matrix\n");
	  if(matrix == NULL)printf("Error in printing matrix\n");
	  else printMatrix(matrix);
	  printf("Nodes in graph\n");
	  DFS(graph,0, &printValue);	  
	};



      if(choice == 2)
	{
	  printf("The number of nodes : %d\n", countVerticesGraph(graph));
	  printf("The number of edges : %d\n", countEdgesGraph(graph)/2);
	  
	}

      if(choice == 3)
	{
	  int row;
	  int column;

	  int n;
	  
	  printf("Input the vertex : \n");
	  printf("Input row : ");

	  scanf("%d", &row);
	  while(getchar() != '\n');
	  printf("Input column: ");
	  scanf("%d", &column);
	  while(getchar()  != '\n');
	  char *nodeName = getNodeName( row, column);
	  printf("node name : \"%s\"\n", nodeName);
	  int nodeID = getVertexID(graph, nodeName);
	  if(nodeID == -1)
	    {
	      printf("No node in row %d and column %d\n", row, column);
	      continue;
	    };
	  int *output = (int *)malloc(sizeof(int) * MAX_ITEM);
	  n = outdegree(graph, nodeID, output);
	  printf("The number of adjacent vertices to node at row %d and column %d: %d\n",row, column, n);
	  int i;
	  for(i = 0; i < n; i++)
	    {
	      printf(" node %d : %s\n", i+1, getVertex(graph, output[i]));
	    };
	  free(output);
	  
	};

      if(choice == 4)
	{
	  Dllist dllist = NULL;
	  int numberOfAdjacent = getLargestOutdegreeVertexIDs(graph, &dllist);
	  printf("number of max adjacent vertices = %d\n", numberOfAdjacent);
	  printf("Found vertices: \n");
	  Dllist ptr;
    
	  dll_traverse(ptr, dllist)
	    {
	      printf("%s\n", getVertex(graph, jval_i(ptr->val)));
	    };
	};

      if(choice == 5)
	{
	  Dllist dllist = NULL;
	  int numberOfZeroAdjacentNodes = getZeroOutdegreeVertexIDs(graph, &dllist);
	  printf("Number of vertices has zero adjacent vertices = %d\n", numberOfZeroAdjacentNodes);
	  Dllist ptr;
	  dll_traverse(ptr, dllist)
	    {
	      printf("%s\n", getVertex(graph, jval_i(ptr->val)));
	    };
	};

      if(choice == 6)
	{
	  int r1, r2, c1, c2;
	  printf("Input first point: \n");
	  printf("Input row of first point: ");
	  scanf("%d", &r1);
	  printf("Input column of first point: ");
	  scanf("%d", &c1);
	  printf("Input second point: \n");
	  printf("Input row of second point: ");
	  scanf("%d", &r2);
	  printf("Input column of second point: ");
	  scanf("%d", &c2);
	  while(getchar() != '\n');
	  int nodeID1 = getVertexID(graph, getNodeName(r1, c1));
	  int nodeID2 = getVertexID(graph, getNodeName(r2, c2));
	  if(nodeID1 == -1 || nodeID2 == -1)
	    {
	      printf("Input is not available\n");
	      continue;
	    };
	  Dllist path = new_dllist();
	  double shortestDistance = shortestPath(graph, nodeID1, nodeID2, path);
	  printf("Shortest distance : %f\n", shortestDistance);
	  if(shortestDistance == INFINITIVE_VALUE)
	    {
	      printf("No way\n");
	      continue;
	    };
	  printf("Path : ");
	  Dllist ptr;
	  dll_traverse(ptr, path)
	    {
	      printf("%s -> ", getVertex(graph, jval_i(ptr->val)));
	    };
	  printf("\n");
	}

      if(choice == 7)
	{
	  
	}
    }
  while(choice != 8);
  return 0;
}
  

/* int countNodesTree(JRB tree) */
/* { */
/*   JRB ptr; */
/*   int count = 0; */
/*   jrb_traverse(ptr, tree) */
/*     { */
/*       count++; */
/*     }; */
/*   return count; */
/* } */


/* int countVerticesGraph(Graph graph) */
/* { */
/*   return countNodesTree(graph.vertices); */
/* } */

/* int countEdgesGraph(Graph graph) */
/* { */
/*   int count = 0; */
/*   JRB ptr; */
/*   jrb_traverse(ptr, graph.edges) */
/*     { */
/*       JRB tempTree = (JRB)jval_v(ptr->val); */
/*       count += countNodesTree(tempTree); */
/*     } */
/*   return count; */
/* } */

/* int getLargestOutdegreeVertexIDs(Graph graph, Dllist *listVertices) */
/* { */
/*   //int maxID = -1; */
/*   int bestValue = 0; */
/*   JRB ptr; */
/*   if(*listVertices == NULL) */
/*     { */
/*       *listVertices = new_dllist(); */
/*     } */
/*   else if(!dll_empty(listVertices)) */
/*     { */
/*       free_dllist(*listVertices); */
/*       *listVertices = new_dllist(); */
/*     }; */
  
/*   jrb_traverse(ptr, graph.edges) */
/*     { */
/*       JRB tree = (JRB) jval_v(ptr->val); */
/*       int n = countNodesTree(tree); */
      
/*       if (n == bestValue) */
/* 	{ */
/* 	  dll_append(*listVertices, ptr->key); */
/* 	} */
/*       else if ( n > bestValue ) */
/* 	{ */
/* 	  bestValue = n; */
/* 	  if(!dll_empty(*listVertices)) */
/* 	    { */
/* 	      free_dllist(*listVertices); */
/* 	      *listVertices = new_dllist(); */
/* 	    }; */
/* 	  dll_append(*listVertices, ptr->key); */
/* 	}; */
/*     }; */
/*   return bestValue; */
/* } */


/* int getZeroOutdegreeVertexIDs(Graph graph, Dllist *listVertices) */
/* { */
/*   if(*listVertices == NULL) */
/*     { */
/*       *listVertices = new_dllist(); */
/*     } */
/*   else if(!dll_empty(*listVertices)) */
/*     { */
/*       free_dllist(*listVertices); */
/*       *listVertices = new_dllist(); */
/*     }; */
/*   int count = 0; */
  
/*   JRB ptr; */
/*   jrb_traverse(ptr, graph.edges) */
/*     { */
/*       JRB tree = (JRB) jval_v(ptr->val); */
/*       if (countNodesTree(tree) == 0) */
/* 	{ */
/* 	  dll_append(*listVertices, ptr->key); */
/* 	  count++; */
/* 	}; */
/*     }; */
  
/*   return count; */
/* } */
